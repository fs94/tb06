package AB01;

public class Addition {

	public static void main(String[] args) {
		double zahl1 = 0.0, zahl2 = 0.0, erg = 0.0;
		
		// Programmhinweis
		hinweis();

		// Eingabe
		zahl1 = eingabe(" 1. Zahl: ");
		zahl2 = eingabe(" 2. Zahl: ");
		
		// Verarbeitung
		erg = verarbeitung(zahl1, zahl2);
		
		// Ausgabe
		ausgabe(zahl1, zahl2, erg);
		
	}
	
	// Programmhinweis
	public static void hinweis() {
		System.out.println("Hinweis: ");
		System.out.println(" Das Programm addiert zwei eingegebene Zahlen ");
	}
	
	// Eingabe
	public static double eingabe(String s) {
		double zahl = 0;
		System.out.println(s);
		zahl = Tastatur.liesDouble();
		return zahl;
	}
	
	// Verarbeitung
	public static double verarbeitung(double zahl1, double zahl2) {
		return (zahl1 + zahl2);
	}
	
	// Ausgabe
	public static void ausgabe(double zahl1, double zahl2, double erg) {
		System.out.println("Ergebnis der Addtion");
		System.out.println(erg + " = " + zahl1 + " + " + zahl2);
	}

}
