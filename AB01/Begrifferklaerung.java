// P Anfang

package AB01;

/**
 * 
 * Begrifferklaerung
 * Beispielprogramm fuer AB01_FuktionenUmrechnungen 1
 * Erklaerung von Methoden und deren Begrifflichkeiten
 * 
 * @author matthias.amelung
 * @version 1.0
 * FS94
 *
 */

public class Begrifferklaerung {

	// HP Anfang
	public static void main(String[] args) {
		
		// D Anfang
		int a = 10;
		// D Ende
		
		beispielMethode(a);		// Methodenaufruf mit Parameter/Argument
								// Aufruf; methode(parameter)

	}
	// HP Ende
	
	
	
	
	/*
	 * Methodendefinition:
	 * 	Modifier Datentyp Methodenname (Parameterliste) {
	 * 		Anweisungen;
	 * 	}
	 * 
	 * Modifier: 	public, private, protected, static,
	 * 				final, abstract, synchronized
	 * 
	 * Datentyp:	R�ckgabewert der Methode. Kann jeder
	 * 				Datentyp sein, auch Objekte und Listen.
	 * 				Soll kein Typ zur�ckgegeben werden: void
	 * 
	 * Methodenname:	Kann freigewaehlt werden. Sollte aber
	 * 					camelCase und identifizierbar sein.
	 * 					Bsp.: 	translateText -- camelCase und identifizierbar
	 * 							tTxt -- camelCase, aber nicht identifizierbar
	 * 
	 * Parameter:	Datentyp Variablenname
	 * 				Varibale vom Datentyp x, welche an die Methode uebergeben wird
	 * 				Nutzung der Varibale in der Methode ueber Variablenname
	 * 
	 */
	
	
	// Methodendefinition Anfang
	public static void beispielMethode(int wert) { // Methodenkopf
		// Methodenrumpf Beginn
		System.out.println(wert);
		// Methodenrumpf Ende
	}
	// Methodendefinition Ende
	
	
}
// P Ende