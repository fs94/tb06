// P Anfang

package AB01;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Urlaub {
	
	// HP Anfang
	public static void main(String[] args) {
		
		// D Anfang
		Scanner sc = new Scanner(System.in);
		DecimalFormat f = new DecimalFormat("#0.00");
		double kursUSD = 0.975;		// Kurs US-Dollar
		double kursGBP = 0.6485;	// Kurs Britische Pfund
		double kursJPY = 118.29;	// Kurs Japanische Yen
		double kursHKD = 7.6292;	// Kurs Hong Kong Dollar
		double budget;				// Reisebudget
		double geldLW = 0;			// Geld Landeswhrung
		double geldFW;				// Geld Fremdwhrung
		double reserve = 0;			// Geld Reserve
		String land;				// Zielland der Reise
		boolean wdh = true;			// Programmwiederholung
		boolean zuViel = true;		// zu viel Geld ausgegeben
		// D Ende
		
		// Budget setzen
		System.out.print("Wie hoch ist ihr Budget? ");
		budget = eingabeEUR(sc);
		
		// Reserve setzen
		System.out.print("\nWollen Sie eine Reserve setzen? (j / n) ");
		if (eingabeText(sc).equalsIgnoreCase("j")) {
			System.out.print("Wie hoch soll die Reserve sein? ");
			reserve = eingabeEUR(sc);
		}
		
		// Programmschleife
		do {
			System.out.println("\n ------------ Whrungsrechner ------------ \n");
			System.out.print("Geben Sie das Land ein, in dass Sie reisen: ");
			land = eingabeText(sc);		// Eingabe Zielland
			
			/* 
			 * Schleife Geldwechseleingabe
			 * Wenn das Budget aufgebraucht ist, wird das Programm beendet
			 * Bei nicht gedeckter Menge wird das Programm wiederholt
			 * Wenn die Reserve unterschritte wird, wird das Programm wiederholt
			 */
			do {
				if (budget == 0) {
					System.out.println("\nSie haben all Ihr Geld ausgegeben.");
					System.out.println("Die Heimreise wird angetreten.");
					System.exit(0);
				} else {
					System.out.print("Wie viel Geld wollen Sie wechseln? ");
					geldLW = eingabeEUR(sc);
					if (geldLW > budget) {
						System.out.println("\nSie haben nicht ausreichend Budget.");
						System.out.println("Die Eingabe wird wiederholt.\n");
						zuViel = true;
					} else if ((budget - geldLW) < reserve) {
						System.out.println("\nDie gewechselte Menge wrde die Reserve unterschreiten.");
						System.out.println("Die Eingabe wird wiederholt.\n");
						zuViel = true;
					} else {
						zuViel = false;
					}
				}
			} while (zuViel);
			
			/*
			 * Funktionsaufrufe entsprechend der land Eingabe
			 * Anschlieende Ausgabe der Geldmenge
			 * Frage, ob Programm wiederholt werden soll
			 */
			if (land.equalsIgnoreCase("USA")) {
				System.out.print("\nWie viel Geld soll gewechselt werden? ");

				geldFW = toUSD(kursUSD, geldLW);
				budget = calcBudget(budget, geldLW);
				
				System.out.println(geldLW + " EUR ensprechen " + f.format(geldFW) + " USD");
			} else if (land.equalsIgnoreCase("Japan")) {
				System.out.print("\nWie viel Geld soll gewechselt werden? ");

				geldFW = toJPY(kursJPY, geldLW);
				budget = calcBudget(budget, geldLW);
				
				System.out.println(geldLW + " EUR ensprechen " + f.format(geldFW) + " JPY");
			} else if (land.equalsIgnoreCase("Hongkong")) {
				System.out.print("\nWie viel Geld soll gewechselt werden? ");

				geldFW = toHKD(kursHKD, geldLW);
				budget = calcBudget(budget, geldLW);

				System.out.println(geldLW + " EUR ensprechen " + f.format(geldFW) + " HKD");
			} else if ((land.equalsIgnoreCase("Grobritannien")) || (land.equalsIgnoreCase("UK"))) {
				System.out.print("\nWie viel Geld soll gewechselt werden? ");

				geldFW = toGBP(kursGBP, geldLW);
				budget = calcBudget(budget, geldLW);

				System.out.println(geldLW + " EUR ensprechen " + f.format(geldFW) + " GBP");
			} else {
				System.out.println("\nZu dem gegebenen Land liegt kein Umrechnungskurs vor.");
				System.out.println("Bitte whlen Sie nur zwischen USA, Japan, Hongkong und Grobritannien.\n");
			} 
			
			System.out.println("Ihr Budget betrgt noch: " + f.format(budget) + " EUR");
			
			System.out.print("\nSoll das Programm wiederholt werden? (j / n) ");
			if (eingabeText(sc).equalsIgnoreCase("n")) {
				wdh = false;
			}
				
		} while (wdh);
		
		// Scanner schlieen
		sc.close();
	}
	// HP Ende
	
	
	
	// Eingabe EURO
	public static double eingabeEUR(Scanner sc) {
		return sc.nextDouble();
	}
	
	// Eingabe Text
	public static String eingabeText(Scanner sc) {
		return sc.next();
	}
	
	// Umrechnung zu USD
	public static double toUSD(double kursUSD, double euro) {
		return (euro*kursUSD);
	}
	
	// Umrechnung zu GPB
	public static double toGBP(double kursGBP, double euro) {
		return (euro*kursGBP);
	}
	
	// Umrechnung zu JPY
	public static double toJPY(double kursJPY, double euro) {
		return (euro*kursJPY);
	}
	
	// Umrechnung zu HKD
	public static double toHKD(double kursHKD, double euro) {
		return (euro*kursHKD);
	}
	
	// Berechnung vom neuen Budget
	public static double calcBudget(double budget, double geldLW) {
		budget -= geldLW;
		return budget;
	}
	
}
// P Ende