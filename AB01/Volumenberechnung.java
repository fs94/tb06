package AB01;

import java.util.Scanner;

public class Volumenberechnung {
		
	public static void main(String[] args) {
	
		// D Anfang
		Scanner sc = new Scanner(System.in);
		// D Ende
		
		
		System.out.println("Volumenberechnung Wuerfel");
		System.out.println("------------------------");
		wuerfelVolumen(sc);
		
		
		System.out.println("Volumenberechnung Quader");
		System.out.println("------------------------");
		quaderVolumen(sc);
		
		
		System.out.println("Volumenberechnung Zylinder");
		System.out.println("--------------------------");
		zylinderVolumen(sc);
		
		
		System.out.println("Volumenberechnung Pyramide");
		System.out.println("--------------------------");
		pyramideVolumen(sc);
		
	}
	
	public static void ausgabe(double erg) {
		System.out.println("Das Volumen betraegt: " + erg + "ccm\n\n");
	}
	
	public static double eingabe(Scanner sc, String a) {
		System.out.print("Geben Sie die " + a + " in cm ein: ");
		double tmp = sc.nextDouble();
		return tmp;
	}
	
	
	/*
	 * V = a * a * a
	 */
	public static void wuerfelVolumen(Scanner sc) {
		String s = "Seitenlaenge";
		double a = eingabe(sc, s);
		
		double vol = a * a * a;
		ausgabe(vol);
	}
	
	/*
	 * V = a * b * c
	 */
	public static void quaderVolumen(Scanner sc) {
		String s = "Seitenlaenge a";
		double a = eingabe(sc, s);
		s = "Seitenlaenge b";
		double b = eingabe(sc, s);
		s = "Seitenlaenge c";
		double c = eingabe(sc, s);
		
		double vol = a * b * c;
		ausgabe(vol);
	}
	
	/*
	 * V = d * d * 3,14 * h / 4
	 */
	public static void zylinderVolumen(Scanner sc) {
		String s = "Groesse des Durchmessers";
		double d = eingabe(sc, s);
		s = "Hoehe";
		double h = eingabe(sc, s);
		
		double vol = d * d * 3.14 * h / 4;
		ausgabe(vol);
	}
	
	/*
	 * V = a * a * h / 3
	 */
	public static void pyramideVolumen(Scanner sc) {
		String s = "Grundseite";
		double a = eingabe(sc, s);
		s = "Hoehe";
		double h = eingabe(sc, s);
		
		double vol = a * a * h / 3;
		ausgabe(vol);
	}
	

}
