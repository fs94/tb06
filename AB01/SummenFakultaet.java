package AB01;

import java.util.Scanner;

public class SummenFakultaet {

	public static void main(String[] args) {
		
		// D Anfang
		Scanner sc = new Scanner(System.in);
		int n;
		int erg;
		double ergAM;
		String s;
		// D Ende
		
		s = "Geben Sie eine Zahl ein, die addiert werden soll: ";
		n = eingabe(sc, s);
		erg = summeN(n);
		s = "Die Summe der Zahl " + n + " beträgt: " + erg + "\n";
		ausgabe(s);
		
		s = "Geben Sie eine Zahl ein, von der die Fakultät gebildet werden soll: ";
		n = eingabe(sc, s);
		erg = fakultaetN(n);
		s = "Die Fakultät der Zahl " + n + " beträgt: " + erg + "\n";
		ausgabe(s);
		
		s = "Geben Sie eine Zahl für das arithmetische Mittel ein.\nBestätigen Sie die Eingabe mit ENTER.\nBeenden Sie die Eingabe mit der Eingabe einer 0.\n";
		ausgabe(s);
		ergAM = arithmetischesMittel(sc);
		s = "Das arithmetische Mittel beträgt: " + ergAM + "\n";
		ausgabe(s);
		
	}
	
	public static void ausgabe(String s) {
		System.out.println(s);
	}
	
	public static int eingabe(Scanner sc, String s) {
		int n;
		System.out.print(s);
		n = sc.nextInt();
		return n;
	}
	
	public static int summeN(int n) {
		int erg = 0;
		for (int i = 0; i <= n; i++) {
			erg += i;
		}
		return erg;
	}
	
	public static int fakultaetN(int n) {
		int erg = 1;
		for (int i = 1; i <= n; i++) {
			erg = erg * i;
		}
		return erg;
	}
	
	public static double arithmetischesMittel(Scanner sc) {
		double erg = 0;
		int a;
		double n = 0;
		
		do {
			a = eingabe(sc, "");
			erg += a;
			n++;
		} while(a != 0);
		erg = 1/(n-1) * erg;
		
		return erg;
	}

}
