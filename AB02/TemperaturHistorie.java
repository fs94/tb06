package AB02;

import java.util.Scanner;

public class TemperaturHistorie {

	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		double mittelwert = 0.0;
					
		int tage = 0;
		System.out.print("Bitte geben Sie die Anzahl der Tage des Monats ein.");
		tage = sc.nextInt();

		double temperatur[] = new double[tage];
		temperatur = eingabe(tage);
			
		mittelwert = durchschnitt(temperatur, tage);
		System.out.println("Die Durchschnittstemperatur betr�gt " + mittelwert);
		
		sc.close();
	}

	public static double[] eingabe(int anz) {
		double tempWerte[] = new double[anz];
		for(int i = 0; i < anz; i++) {
			System.out.print((i+1) + ". Temperaturwert eingeben: ");
			tempWerte[i] = sc.nextDouble();
		}
		return tempWerte;
	}

	public static double durchschnitt(double werte[], int anz) {
		double durchschnitt = 0.0;
		for(int i = 0; i < anz; i++) {
			durchschnitt = durchschnitt + werte[i];
		}	
		durchschnitt = durchschnitt / anz;
		return durchschnitt;
	}
}
