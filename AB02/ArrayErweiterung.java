package AB02;

import java.util.Scanner;

public class ArrayErweiterung {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		final int ANZ = 5;
		int ANZ_PLUS;
		
		double[] arrayOrig = new double[ANZ];
		double[] arrayPlus;
		
		System.out.print("Geben Sie die neue Groesse des Arrays ein: ");
		ANZ_PLUS = sc.nextInt();
		
		// Populating arrayOrig with test data
		for (int i = 0; i < arrayOrig.length; i++) {
			arrayOrig[i] = i+1;
		}
		
		arrayPlus = extendArray(arrayOrig, ANZ_PLUS);
		
		// output test data and array size
		System.out.println("\nGroesse altes Array: " + arrayOrig.length);
		System.out.println("Groesse neues Array: " + arrayPlus.length + "\n");
		for (int i = 0; i < arrayPlus.length; i++) {
			if (i < arrayOrig.length) {
				System.out.println((i+1) + ". Stelle altes Array: " + arrayOrig[i]);
				System.out.println((i+1) + ". Stelle neues Array: " + arrayPlus[i] + "\n");
			} else {
				System.out.println((i+1) + ". Stelle neues Array: " + arrayPlus[i] + "\n");
			}
		}
		
		// Scanner schließen
		sc.close();

	}
	
	public static double[] extendArray(double[] arrayAlt, int size) {
		double[] arrayNeu = new double[size];
		for (int i = 0; i < arrayAlt.length; i++) {
			arrayNeu[i] = arrayAlt[i];
		}
		return arrayNeu;
	}
	
}
